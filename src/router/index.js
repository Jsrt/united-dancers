import Vue from 'vue'
import VueRouter from 'vue-router'
import Course from '../views/Course.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'public',
    component: () => import(/* webpackChunkName: "" */ '../views/Public.vue'),
    children: [
      {
        path: '/',
        name: 'home',
        component: () => import(/* webpackChunkName: "" */ '../views/@public/Home.vue')
      },
      {
        path: 'courses',
        name: 'courses',
        component: () => import(/* webpackChunkName: "" */ '../views/@public/Courses.vue')
      },
      {
        path: 'teachers',
        name: 'teachers',
        component: () => import(/* webpackChunkName: "" */ '../views/@public/Teachers.vue')
      },
      {
        path: '/view-course/:name/:id',
        name: 'previewCourse',
        component: () => import(/* webpackChunkName: "" */ '../views/@public/PreviewCourse.vue')
      },
      {
        path: 'checkout',
        name: 'checkout',
        component: () => import(/* webpackChunkName: "" */ '../views/@public/Checkout.vue'),
        props: true
      },
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "" */ '../views/@public/LoginPay.vue')
      },
      {
        path: '/profile/:id',
        name: 'profile',
        component: () => import(/* webpackChunkName: "" */ '../views/@public/Profile.vue'),
        meta: { requiresAuth: true }
      }
    ]
  },
  {
    path: '/course',
    name: 'course',
    component: Course
  },
  {
    path: '/admin',
    name: 'admin',
    component: () => import(/* webpackChunkName: "" */ '../views/Admin.vue'),
    children: [
      {
        path: '/',
        name: 'dashboard',
        component: () => import(/* webpackChunkName: "" */ '../views/@admin/Dashboard.vue')
      },
      {
        path: 'users',
        name: 'users',
        component: () => import(/* webpackChunkName: "" */ '../views/@admin/Users.vue')
      }
    ]
  },
  {
    path: '**',
    redirect: '/'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// router.beforeEach((to, from, next) => {
//   if (to.meta.requiresAuth) {
//     const authUser = localStorage.getItem('token')
//     console.log(authUser)
//     if (authUser) {
//       next()
//     } else {
//       next({ name: 'home' })
//     }
//   }
//   next()
// })

export default router
