let courses = []

let cart = {
  total: 0,
  subtotal: 0,
  courses
}

function initialize (course) {
  const storeData = JSON.parse(localStorage.getItem('cart'))
  if (storeData !== null) {
    cart = storeData
  }
  return cart
}

function manageCourse (course) {
  // obtener cantidad de cursos en el carrito
  const courseTotal = cart.courses.length
  // comprobamos si tenemos cursos
  if (courseTotal === 0) {
    // Añadiendo primer curso
    cart.courses.push(course)
  } else { // si tenemos cursos hacemos lo siguiente
    let actionUpdateOk = false
    for (let i = 0; i < courseTotal; i++) {
      // comprobamos que coincide el producto con alguno de la lista
      if (course._id === cart.courses[i]._id) {
        console.log('Borrar item seleccionado')
        // quitar elemento
        cart.courses.splice(i, 1)
        actionUpdateOk = true
        i = courseTotal
      }
    }
    if (!actionUpdateOk) {
      cart.courses.push(course)
    }
  }
  checkoutTotal()
  return cart
}

function checkoutTotal () {
  let subtotal = 0
  let total = 0
  cart.courses.map((course) => {
    total += course.precio // total
    subtotal += course.precio
  })
  cart.total = total
  cart.subtotal = subtotal
  console.log(cart, 'calculado')
  setInfo()
}

function clear () {
  courses = []
  cart = {
    total: 0,
    subtotal: 0,
    courses
  }
  setInfo()
  console.log('Hemos borrado la información')
  return cart
}

function setInfo () {
  localStorage.setItem('cart', JSON.stringify(cart))
}

function openNav () {
  document.getElementById('mySidenav').style.width = '500px'
  document.getElementById('overlay').style.display = 'block'
}

function closeNav () {
  document.getElementById('mySidenav').style.width = '0'
  document.getElementById('overlay').style.display = 'none'
}

export { openNav, closeNav, initialize, manageCourse, clear }
