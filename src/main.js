import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueGlide from 'vue-glide-js'
import 'vue-glide-js/dist/vue-glide.css'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import { ValidationProvider } from 'vee-validate'
import Notifications from 'vue-notification'
// custom public components
import appHeader from './components/@public/header.vue'
import appFooter from './components/@public/footer.vue'
import shoppingCart from './components/@public/shoppingCart.vue'
// custom admin components
import admHeader from './components/@admin/header.vue'
import admSidebar from './components/@admin/sidebar.vue'

Vue.config.productionTip = false

Vue.use(VueGlide)
Vue.use(Buefy)
Vue.use(Notifications)

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('app-footer', appFooter)
Vue.component('app-header', appHeader)
Vue.component('app-shopping-cart', shoppingCart)

// register admin componets
Vue.component('adm-header', admHeader)
Vue.component('adm-sidebar', admSidebar)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
